import sys

sys.path.insert(0, 'include/')
# This function will return a single random rule-function as a string
from rules import GetRandomRule
from random import randint


class SingleMultiple(object):
    """This is a template for creating an evolutionary algorithms.
	All of the algorithms included in this directory will be used by the engine if self.active is set to True"""

    def __init__(self):
        # These are variables that have to be set
        self.name = "singleMultipleScore"
        self.active = False  # This should be set to True if the engine should use this algorithm


        # These variables will be updated by the engine before the generate_rules() method is invoked each round
        self.last_round_score = 0.0  # This is a float that represents how well the last set of submitted rules did
        self.user_rules = []  # This is a list of rules that the user may have submitted to the main engine to be evolved. Note that this may be empty.
        self.generation_number = 0
        self.engine = None


    def generate_rules(self, number_of_rules_required, successful_rules):
        rule_score = {}
        for successful_rule in successful_rules:
            # print successful_rule
            if successful_rule.rule not in rule_score:
                rule_score[successful_rule.rule] = successful_rule.score
        rule_score_sorted = sorted(rule_score.items(), key=operator.itemgetter(1))  # , reverse=True)
        gen_rules = BreedSingleMultiple(rule_score_sorted, number_of_rules_required)
        return gen_rules
    # def generate_rules(self, number_of_rules_required, successful_rules):
    # 	# open('/tmp/hello{0}'.format(randint(0,1000)), 'w')
    # 	results = []
    # 	while number_of_rules_required > 0:
    # 		# print str(number_of_rules_required)
    # 		results.append(GetRandomRule(randint(1,16)).strip())
    # 		number_of_rules_required-=1
    #
    # 	return results





    #
    # Custom methods can be defined below.
    #

    def my_method(self, variable):
        return variable


import operator


def BreedSingleMultiple(rules_scores, population_max):
    tmp_rules_scores = []
    tmp_rules_scores = rules_scores
    new_rules_parents = []
    new_rules = []
    bred_rules_dict = {}
    if len(tmp_rules_scores) < 2:
        return []
    # Breeding selection process
    couple = []
    couples = []
    while len(tmp_rules_scores) > 1:
        chance = randint(0, 100)
        lower = 0
        upper = len(tmp_rules_scores) - 1
        # if chance < 50:
        # upper = int(len(tmp_rules_scores)*0.05)
        # else:
        # upper = len(tmp_rules_scores) - 1
        index = randint(lower, upper)
        parent_X = tmp_rules_scores[index]
        tmp_rules_scores.remove(parent_X)
        if len(couple) < 2:
            couple.append(parent_X[0])
        elif len(couple) == 2:
            couples.append(couple)
            couple = [];
            couple.append(parent_X[0])

    # Breeding process
    for couple in couples:
        children = []
        # Single crossover point
        parent_a = couple[0].split(' ')
        parent_b = couple[1].split(' ')
        crossover_a = randint(0, len(parent_a) - 1)
        crossover_b = randint(0, len(parent_b) - 1)
        children.append(parent_a[:crossover_a] + parent_b[crossover_b:])
        children.append(parent_a[crossover_a:] + parent_b[:crossover_b])
        children.append(parent_b[:crossover_b] + parent_a[crossover_a:])
        children.append(parent_b[crossover_b:] + parent_a[:crossover_a])
        # Multiple crossover points
        if len(parent_a) > 2 and len(parent_b) > 2:
            if randint(0, 1) == 1:
                number_of_crossovers_a = randint(0, len(parent_a) - 1)
                for index in range(1, number_of_crossovers_a):
                    crossover = randint(0, (len(parent_a) and len(parent_b)))
                    children.append(parent_a[:crossover] + parent_b[crossover:])
                    children.append(parent_a[crossover:] + parent_b[:crossover])
                    children.append(parent_b[:crossover] + parent_a[crossover:])
                    children.append(parent_b[crossover:] + parent_a[:crossover])
            if randint(0, 1) == 0:
                number_of_crossovers_b = randint(0, len(parent_b) - 1)
                for index in range(1, number_of_crossovers_b):
                    crossover = randint(0, (len(parent_a) and len(parent_b)))
                    children.append(parent_a[:crossover] + parent_b[crossover:])
                    children.append(parent_a[crossover:] + parent_b[:crossover])
                    children.append(parent_b[:crossover] + parent_a[crossover:])
                    children.append(parent_b[crossover:] + parent_a[:crossover])
        for child in children:
            if len(child) < 16 and len(child) > 0 and (' '.join(child)) not in new_rules:
                new_rules.append(' '.join(child))
                # rule_parents = [' '.join(child), ' '.join(parent_a), ' '.join(parent_b)]
                # new_rules_parents.append(rule_parents)
                # bred_rules_dict[' '.join(child)] = ['SingleAndMultipleCrossoverAlgorithm', ' '.join(parent_a), ' '.join(parent_b)]
            if len(new_rules) >= population_max:
                return new_rules
    return new_rules
