import sys
sys.path.insert(0, 'include/')
#This function will return a single random rule-function as a string
from rules import GetRandomRule

'''
The engine is an API that provides the following functionality:

	engine.getAllSuccessfulRules() #This returns all of the rules that have cracked something so far, in order of their successfulness
	engine.getUserRules() #This is a list of rules that the user may have submitted to the main engine to be evolved. Note that this may be empty.
'''

class myBreedingAlgorithm(object):
	"""This is a template for creating an evolutionary algorithms. 
	All of the algorithms included in this directory will be used by the engine if self.active is set to True"""
	def __init__(self):
		#These are variables that have to be set
		self.name = ""
		self.active = False #This should be set to True if the engine should use this algorithm
		

		#These variables will be updated by the engine before the generate_rules() method is invoked each round
		self.last_round_score = 0.0 #This is a float that represents how well the last set of submitted rules did
		self.generation_number = 0
		self.engine = None

		#Any other variables can be declared here, they will not be used by the main engine in any way. 
		self.myVar = 0
		self.myVar2 = "This is a string"

	'''
		This should return a list containing at most, the number of rules specified by number_of_rules_required.
		Note that this function will be called by the engine once per generation. This is where the magic should happen.
		Because the main engine will dynamically control the number of rules that it requires from each algorithm, this will vary from round to round.
	'''
	def generate_rules(self, number_of_rules_required, successful_rules):
		return []

	#
	# Custom methods can be defined below.
	#

	def my_method(self, variable):
		return variable