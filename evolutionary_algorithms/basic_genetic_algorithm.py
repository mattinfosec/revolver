import sys
sys.path.insert(0, 'include/')
# from statistics import mean, pstdev
from random import randint
from rules import GetRandomRule
import logging
import operator
logging.basicConfig(filename='dynamic_selection_pressure.log', level=logging.DEBUG)

class basic_ga():
    """This is a template for creating an evolutionary algorithms. 
    All of the algorithms included in this directory will be used by the engine if self.active is set to True"""
    def __init__(self):
        #These are variables that have to be set
        self.name = "basic"
        self.active = True #This should be set to True if the engine should use this algorithm
        
        #These variables will be passed to the class before the run() method is invoked
        self.all_successful_rules = [] #This is a list that contains all of the successful rules that have been seen so far by the main engine
        self.last_round_score = 0.0 #This is a float that represents how well the last set of submitted rules did
        self.user_rules = [] #This is a list of rules that the user may have submitted to the main engine to be evolved. Note that this may be empty.
        self.generation = 0

        #Any other variables can be declared here, they will not be used by the main engine in any way. 
        self.selection_pressure = 0.7
        self.previous_round_scores = []
        self.breeding_pool = []

        #5% mutation chance
        self.mutation_threshold = 5

    '''
        This should return a list containing at most, the number of rules specified by number_of_rules_required.
        Note that this function will be called by the engine once per generation. This is where the magic should happen.
        Because the main engine will dynamically control the number of rules that it requires from each algorithm, this will vary from round to round.
    '''
    def generate_rules(self, number_of_rules_required, all_successful_rules):
    	logging.debug("Need {0}".format(number_of_rules_required))
        self.all_successful_rules = all_successful_rules
        self.number_of_rules_required = int(number_of_rules_required)
        self.select_breeding_partners()

        #Everything is done. Just need to breed
        return_rules = []
        if len(self.breeding_pool) > 0:

            while len(return_rules) < self.number_of_rules_required:
                temp_rules=self.breed()

                rule = ""
                for i in temp_rules:
                        rule = ''
                        for j in i:
                            if j != u' ' and len(rule.split(' '))<=16:
                                rule+=j + ' '

                        return_rules.append(rule.strip())
        # print return_rules
        return return_rules


    #Custom methods can be defined below.
    def select_breeding_partners(self):
        self.previous_round_scores.append(self.last_round_score)
        #Next we create the pool to breed from
        self.breeding_pool = self.get_pool()
        

    def breed(self):
        partner1 = self.breeding_pool[randint(0,len(self.breeding_pool)-1)].split(" ")
        partner2 = self.breeding_pool[randint(0,len(self.breeding_pool)-1)].split(" ")
        children = []
        #Single Crossover
        crossover_a = randint(0, len(partner1) - 1)
        crossover_b = randint(0, len(partner2) - 1)

        children.append(self.mutation(partner1[:crossover_a] + partner2[crossover_b:]))
        children.append(self.mutation(partner1[crossover_a:] + partner2[:crossover_b]))
        children.append(self.mutation(partner2[:crossover_b] + partner1[crossover_a:]))
        children.append(self.mutation(partner2[crossover_b:] + partner1[:crossover_a]))
        final_children = []
        for i in children:
            if len(i)>15:
                final_children.append(i[:16])
            else:
                final_children.append(i)
       
        return final_children


    #This should be a rule in a list separated by spaces. Very simple mutation for now
    def mutation(self, rule):
        newrule = []
        for i in rule:
            fate = randint(0,100)
            if fate <= self.mutation_threshold:
                newrule.append(GetRandomRule(1).strip())
            else:
                newrule.append(i.strip())
        #logging.debug("dynamicselectionpressure: {0} became {1}".format(newrule, rule))
        return newrule


    #Increases selection pressure if things are going well, when the last round score is less than the 
    #mean + stddev of the last_10_rounds, decrease pressure
    


    def get_pool(self):

        list_length = len(self.all_successful_rules)
        #This should the number that we scrape off the top of the best rules
        number = self.selection_pressure * list_length
    	logging.debug("dynamicselectionpressure:  The pool size is going to be {0}".format(int(number)))
    	result = []
    	for i in  self.all_successful_rules[:int(number)]:
    		if i.rule != u' ':
    			result.append(i.rule)
        return result