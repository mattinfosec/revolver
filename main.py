# DEFAULTS
default_project_dir = 'projects/'
default_tmp_dir = 'tmp/'
default_population = 1000
default_max_iteration = 100
# path_to_hashcat = './hashcat-cli32.bin'
# path_to_hashcat = './hashcat/hashcat-cli64.bin'
from multiprocessing import Process
import operator
import os
import sys
sys.path.insert(0, 'include/')
import db
import user
import rules
import files

def Init(user_input):
    # Check existence of default directories
    if not os.path.isdir(default_tmp_dir):
        os.system('mkdir ' + default_tmp_dir)
    if not os.path.isdir(default_project_dir):
        os.system('mkdir ' + default_project_dir)

    # Set project files
    user_input['project_dir'] = default_project_dir + user_input['project_name']
    user_input['project_file'] = default_project_dir + user_input['project_name'] + '/rules.db'
    user_input['project_pot'] = default_project_dir + user_input['project_name'] + '/cracked.pot'
    user_input['current_rule_file'] = default_tmp_dir + user_input['project_name'] + '_current.rules'
    user_input['debug_file'] = default_tmp_dir + user_input['project_name'] + '_debug.info'

    # Check if project exists
    if not os.path.isdir(user_input['project_dir']):
        if user_input['population'] == 0:
            user_input['population'] = default_population
        if user_input['max_iteration'] == 0:
            user_input['max_iteration'] = default_max_iteration
        os.system('mkdir ' + user_input['project_dir'] + ' ' + user_input['project_dir'] + '/debug; cp ' + user_input['hash_file'] + ' ' + user_input['project_dir'] + '/tocrack.hash')
        user_input['hash_file'] = user_input['project_dir'] + '/tocrack.hash'
        return db.HandleConfig('create', user_input)
    else:
        # If project exists and user passed new arguments --> update config
        if os.path.isfile(user_input['project_pot']):
            os.system('cp ' + user_input['project_pot'] + ' hashcat.pot')
        if (user_input['word_list'][0] != '' or user_input['user_rule_file'] != '' or user_input['population'] != 0 or user_input['max_iteration'] != 0) and (user_input['hash_file'] == '' and user_input['hash_type'] == ''):
            return db.HandleConfig('update_project', user_input)
        elif user_input['hash_file'] != '' or user_input['hash_type'] != '':
            print 'No support for changing hash file or hash type'
            quit()
        else: # Get existing project config
            return db.HandleConfig('get', user_input)
import string
def GetComplexity(password):
    max_complexity = len(password) ** len(string.printable)
    complexity = 0
    # if password.isdigit():
    #     # max_complexity = (len(password) ** len(string.digits)) * 1.0
    #     complexity = (1.0 * (len(string.digits) ** len(password))) / max_complexity
    # else:
        # max_complexity = (len(password) ** len(string.printable)) * 1.0
    lower = 0
    upper = 0
    digit = 0
    special = 0
    for cha in password:
        if cha in string.ascii_lowercase:
            lower += 1
        elif cha in string.ascii_uppercase:
            upper += 1
        elif cha in string.digits:
            digit += 1
        elif cha in string.printable:
            special += 1
        # max = 0
        # if lower > 0:
        #     max = len(string.ascii_lowercase)
        # if upper > 0:
        #     max += len(string.ascii_uppercase)
        # if digit > 0:
        #     max += len(string.digits)
        # if special > 0:
        #     max += len(string.printable) - len(string.digits) - len(string.ascii_letters)
        # max_complexity = 1.0 * (max ** len(password))
        complexity = (1.0 * (len(string.ascii_lowercase) ** lower) * (len(string.ascii_letters) ** upper) * (len(string.ascii_letters + string.digits) ** digit) * (len(string.printable) ** special)) / max_complexity

    return complexity

'''This will return False if the rule had no effect at all, otherwise it will return the trimmed rule with only items that actually affected the root word'''
def prune_rule(rule, root_password, altered_password):
    if root_password == altered_password:
        return False
    simple_rules = ['l', 'c', 'C', 't', 'r', 'd', 'f', '{', '}', '[',']', 'q', 'k', 'K', 'E','$','^',')','(']
    position_rules = ['TN', 'LN', 'RN', '+N', '-N', '.N', ',N', 'DN', "'N"]
    position_rules_without_argument = ['T', 'L', 'R', '+', '-', '.', ',', 'D', "'"]
    duplicate_rules = ['yN', 'YN', 'zN', 'ZN', 'pN']
    duplicate_rules_without_argument = ['y', 'Y', 'z', 'Z', 'p']
    NM_rules = ['xNM', '*NM']
    NM_rules_without_argument = ['x', '*']
    X_rules = ['$X', '^X', '@X','X', '(X']
    X_rules_without_argument = ['@']
    XY_rules = ['sXY']#, '*XY']
    NX_rules = ['iNX', 'oNX']
    NX_rules_without_argument = ['i', 'o']
    altered_rule = ''
    rule = rule.split(" ")
    for function in rule:
        if function in simple_rules:
            altered_rule += function + " "
            continue
        if len(function) > 1:
            #Check if it is in position rules
            if function[0] in simple_rules:
                altered_rule+=function + " "
                continue
            if function[0] in position_rules_without_argument and len(root_password) >= len(function[1]):
                altered_rule+=function + " "
                continue
            if function[0] in duplicate_rules_without_argument and len(root_password) >= len(function[1]):
                altered_rule+=function + " "
                continue
            if function[0] in NM_rules_without_argument:
                if function[0] == 'x':
                    if int(function[1]) <= len(root_password):
                        altered_rule+=function + " "
                        continue
                    else:
                        continue
                elif function[0] == '*':
                    if function[1] in root_password:
                        altered_rule+=function + " "
                        continue
                    else:
                        continue

                altered_rule+=function + " "
                continue
            if function[0] in X_rules_without_argument:
                if function[0] == '@' and function[1] in root_password:
                    altered_rule+=function + " "
                    continue
                else:
                    continue

            if function[0] == 's' and function[1] in root_password:
                altered_rule+=function + " "
                continue
            if function[0] in NX_rules_without_argument:
                if int(function[1]) <= len(root_password):
                    altered_rule+=function + " "
                    continue
                else:
                    continue
    if len(altered_rule.strip()) == 0:
        return False
    else:
        # print "Pruned rule from {0} to {1} root word {2} password {3}".format(rule, altered_rule, root_password, altered_password)
        return altered_rule.strip()

class Project():
    def __init__(self):
        self.config = Init(user.GetInput())
        self.current_rules = [] # list of current rule objects
        self.current_rules_scores = {} # rule: score
        self.current_rules_complexity = {} # rule: complexity
        self.statistics = [] # list of statistics for each generated type for current iteration

        self.round_cracked = 0
        self.cracked_by_rule = []
        self.cracked_total = []

        self.pruned_rules = {}
    def Generate(self):
        print 'main/Project: generate'
        self.config = db.UpdateState(self.config, 'generate')

        # if user passed new rule file, remove any rules in generated rules table and copy rules to current_rules_file
        # TODO parse any rule file passed by the user to ensure that individual rules are space separated and do not contain any ':'!
        if self.config.user_rule_file != '':
            print 'main/Project/Generate: handling user rules'
            self.current_rules = rules.HandleUserRules(self.config) # copy user rules to current rules and save to current_rule_file
            self.config = db.UpdateUserRules(self.config) # set user rule file to ''
        else:
            # Generate new rules
            # self.current_rules = rules.Generate(self.config, self.generated_rules, self.successful_rules)
            eng = rules.Engine(self.config)
            self.current_rules = eng.Generate()

    def Crack(self, wordlist):
        print 'main/Project: crack'
        self.config = db.UpdateState(self.config, 'crack')
        # hashcat_arg = path_to_hashcat + ' -m ' + self.config.hash_type + ' --debug-file ' + self.config.debug_file + ' --debug-mode=4 --rules ' + self.config.current_rule_file + ' ' + self.config.hash_file + ' ' + wordlist + ' >/dev/null 2>&1'
        hashcat_arg = user.path_to_hashcat + ' --potfile-path=hashcat.pot -m ' + self.config.hash_type + ' --debug-file ' + self.config.debug_file + ' --debug-mode=4 --rules ' + self.config.current_rule_file + ' ' + self.config.hash_file + ' ' + wordlist + ' >/dev/null 2>&1'
        print (hashcat_arg)
        os.system(hashcat_arg)
        os.system('mv hashcat.pot hash.tmp; cat hash.tmp | sort | uniq > hashcat.pot; rm hash.tmp; cat hashcat.pot >> ' + self.config.project_pot)
        os.system('cat hashcat.pot | cut -d ":" -f1 | sort -u > cracked.hash')
        os.system('cat ' + self.config.hash_file + ' | sort -u > tocrack.hash')
        os.system('comm -23 tocrack.hash cracked.hash > ' + self.config.hash_file )
        os.system('rm hashcat.pot cracked.hash tocrack.hash')

    def Harvest(self, wordlist):
        print 'main/Project: harvest'
        self.config = db.UpdateState(self.config, 'harvest')
        # TODO harvest, save only part of rules responsible for cracking
        debug_lines = files.ReadFromFile(self.config.debug_file)
        # cracked_by_rule = []
        # cracked_total = []
        if debug_lines != 0:
            pass_complexity_dict = {}
            self.pruned_rules = {}
            for debug_line in debug_lines:

                line_list = debug_line.split(':')
                try:
                    try:
                        root_word = line_list[0].strip() # POTENTIAL UNICODE ISSUES --> TEST
                    except:
                        root_word = unicode(line_list[0].strip(), errors='ignore')
                    rule = line_list[1].strip()
                    password = line_list[2].strip()
                    ####################################################################################################
                    rule_pruned = prune_rule(rule, root_word, password)
                    if rule_pruned != False:
                        if rule not in self.pruned_rules:
                            self.pruned_rules[rule] = [rule_pruned]
                        else:
                            if rule_pruned not in self.pruned_rules[rule]:
                                self.pruned_rules[rule].append(rule_pruned)

                    if password not in self.cracked_total:
                        self.cracked_total.append(password)
                    if root_word != password:
                        if password not in self.cracked_by_rule:
                            self.cracked_by_rule.append(password)
                        complexity = GetComplexity(password)
                        if password not in pass_complexity_dict:
                            pass_complexity_dict[password] = complexity
                        elif password in pass_complexity_dict:
                            pass_complexity_dict[password] += complexity
                        if rule not in self.current_rules_complexity and rule != '':
                            self.current_rules_complexity[rule] = complexity
                        elif rule in self.current_rules_complexity and rule != '':
                            self.current_rules_complexity[rule] += complexity
                        if rule not in self.current_rules_scores and rule != '':
                            self.current_rules_scores[rule] = 1
                        elif rule in self.current_rules_scores and rule != '':
                            self.current_rules_scores[rule] += 1

                except:
                    print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
                    print line_list
            # pass_complexity_sorted = sorted(pass_complexity_dict.items(), key=operator.itemgetter(1), reverse=True)
            # for pass_comp in pass_complexity_sorted:
            #     print pass_comp

                # if root_word != password:
                #     if rule not in self.current_rules_scores and rule != '':
                #         self.current_rules_scores[rule] = 1
                #     elif rule in self.current_rules_scores and rule != '':
                #         self.current_rules_scores[rule] += 1
                    # if root_word not in self.current_words_scores:
                    #     self.current_words_scores[root_word] = [1, wordlist]
                    # elif root_word in self.current_words_scores:
                    #     self.current_words_scores[root_word][0] += 1
                    # if password not in self.current_cracked:
                    #     self.current_cracked[password] = [rule, root_word, wordlist]
            # os.system('mv ' + self.config.debug_file + ' ' + self.config.project_dir + 'debug/' + str(self.config.iteration) + '.debug')
            os.system('cat ' + self.config.debug_file + ' >> ' + self.config.project_dir + '/debug/' + str(self.config.iteration) + '.debug; rm ' + self.config.debug_file)
            print 'TOTAL CRACKED: ' + str(len(self.cracked_total)) + ' CRACKED BY RULES: ' + str(len(self.cracked_by_rule))
            # self.round_cracked = len(self.cracked_by_rule)
    def Update(self):
        print 'main/Project: update iteration'
        self.config = db.UpdateState(self.config, 'update')

        # TODO output single pruned rule and fix continue
        for rule in self.pruned_rules.keys():
            # backup_rule = rule.split(' ')
            original_rule_list = rule.split(' ')
            # print 'Original rule: ' + rule
            if len(self.pruned_rules[rule]) > 1:
                rule_list = rule.split(' ')
                for pruned_rule in self.pruned_rules[rule]:
                    # print '             pruned rule: ' + pruned_rule
                    pruned_rule_list = pruned_rule.split(' ')
                    for gene in pruned_rule_list:
                        if gene in rule_list:
                            rule_list.remove(gene)
                            # print rule_list
                for gene in rule_list:
                    original_rule_list.remove(gene)
                # print 'Original rule: ' + rule + '      ====================>       ' + ' '.join(original_rule_list)
                self.pruned_rules[rule] = ' '.join(original_rule_list)
                # print '*Rule: ' + rule + ' Updated to: ' + self.pruned_rules[rule]
            else:
                self.pruned_rules[rule] = self.pruned_rules[rule][0]
                # print '#Rule: ' + rule + ' Updated to: ' + self.pruned_rules[rule]
                # print 'Final rule: ' + rule + '     =========>      ' + self.pruned_rules[rule][0]
                # continue

        # get rule type without querying db
        current_rule_dict = {} # { rule: [type]}
        generation_types = []
        # new_rule_score_dict = {}
        # new_rule_complexity_dict = {}
        for current_rule in self.current_rules:
            # if current_rule.rule in self.pruned_rules.keys():
            #     new_rule_score_dict[self.pruned_rules[current_rule.rule]] = self.current_rules_scores[current_rule.rule]
            #     new_rule_complexity_dict[self.pruned_rules[current_rule.rule]] = self.current_rules_scores[current_rule.rule]
            #     # print 'Original: ' + current_rule.rule + ' Changed to: ' + self.pruned_rules[current_rule.rule]
            #     current_rule.rule = self.pruned_rules[current_rule.rule]
            if current_rule.type not in generation_types:
                generation_types.append(current_rule.type)
            current_rule_dict[current_rule.rule] = [current_rule.type] #, current_rule.related_a, current_rule.related_b]

        # self.current_rules_scores = new_rule_score_dict
        # self.current_rules_complexity = new_rule_complexity_dict

        # save current successful rules to successful rules table and return updated successful_rules object list
        current_rules_scores_sorted = sorted(self.current_rules_scores.items(), key=operator.itemgetter(1), reverse=True)
        type_score_dict = db.UpdateSuccessfulRules(self.config, current_rules_scores_sorted, current_rule_dict, self.current_rules_complexity, self.pruned_rules)
        # get current iteration statistics
        # self.statistics = db.UpdateStatistics(self.config.iteration, generation_types, len(self.cracked_total), type_score_dict)
        self.statistics = db.UpdateStatistics(self.config.iteration, generation_types, len(self.cracked_total), type_score_dict)
        # save current rules to generated rules table and return updated generated_rules list
        db.UpdateGeneratedRules(self.current_rules)#, self.generated_rules)

        # increment iteration
        self.config = db.UpdateIteration(self.config)

        # reset all current items to null
        self.current_rules = [] # list of current rule objects
        self.current_rules_scores = {} # rule: score
        self.current_rules_complexity = {}
        self.round_cracked = 0
        self.cracked_by_rule = []
        self.cracked_total = []


    def Plot(self):
#         chart_1_data = db.GET_top_10_root_words()
        chart_1_data = []
        chart_2_data = db.GET_top_10_rules()
        chart_3_data = []
        chart_4_data = []
        for i in range(0, self.config.iteration):
            chart_3_data.append((str(i), db.GET_cracked(i, self.config.project_pot)))
            stats = db.GET_statistics(i)
            for stat in stats:
                chart_4_data.append((stat.type + '_' + str(i), stat.total_cracked))

        print chart_3_data

        p = Process(target=graph, args=(chart_1_data, chart_2_data, chart_3_data, chart_4_data))
        print chart_4_data

        p.start()
#
#     # no need for this
#     # def __repr__(self):
#     #     return "<Project(config='%s')>" % (self.config)
import requests
def graph(chart_1, chart_2, chart_3, chart_4):
    chart_1_URL = 'http://localhost:8080/generateChart1'
    chart_2_URL = 'http://localhost:8080/generateChart2'
    chart_3_URL = 'http://localhost:8080/generateChart3'
    chart_4_URL = 'http://localhost:8080/generateChart4'
    # chart_1_URL = 'http://192.168.97.144:8080/generateChart1'
    # chart_2_URL = 'http://192.168.97.144:8080/generateChart2'
    # chart_3_URL = 'http://192.168.97.144:8080/generateChart3'
    # chart_4_URL = 'http://192.168.97.144:8080/generateChart4'

    requests.get(chart_1_URL, params=chart_1)
    requests.get(chart_2_URL, params=chart_2)
    requests.get(chart_3_URL, params=chart_3)
    requests.get(chart_4_URL, params=chart_4)

def Main():
    project_object = Project()
    if project_object.config.wordlist_type == 'dir':
        dir_list = os.listdir(project_object.config.wordlist)
        wordlists = []
        for file in dir_list:
            wordlists.append(project_object.config.wordlist.strip('/') + '/' + file)
    else:
        wordlists = [project_object.config.wordlist]
    # # TODO resume state if program closed prematurely
    # print project_object.config.state
    if project_object.config.state != 'init':
        project_object.current_rules = db.GetCurrentRules()
        for wordlist in wordlists:
            project_object.Crack(wordlist)
            project_object.Harvest(wordlist)
        project_object.Update()
    for i in range(project_object.config.iteration, project_object.config.iteration + project_object.config.max_iteration):
        print '================================================= Iteration: ' + str(project_object.config.iteration) + ' ======================================================='
        project_object.Generate()
        for wordlist in wordlists:
            print '********************* Wordlist: ' + wordlist + ' *********************'
            project_object.Crack(wordlist)
            project_object.Harvest(wordlist)

        project_object.Update()
        stats = project_object.statistics
        for stat in stats:
            print stat
        project_object.Plot()
Main()
