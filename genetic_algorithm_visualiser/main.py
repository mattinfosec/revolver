
import os
import cherrypy
import json
import requests
from random import randint
#######################################################
#
# Cherrpy webserver
#
#######################################################


chart1=""
chart2=""
chart3=""
chart4=""
chart5=""
chart6=""
chart7=""
colors = []

generation_count = 1
def test():
    requests.get("http://localhost:8080/generateChart1?password=900&chocolate=45&pass=2&qwerty=1")
    requests.get("http://localhost:8080/generateChart2?$1=900&se3=45")
    requests.get("http://localhost:8080/generateChart3?1=9&2=45&3=65&4=234&5=345&6=12")
    requests.get("http://localhost:8080/generateChart4?bred_1=20&genetic_1=5&random_1=32&bred_2=25&genetic_2=20&random_2=5&bred_3=30&genetic_3=47&random_3=5&bred_4=35&genetic_4=67&random_4=8")
class index(object):
    @cherrypy.expose
    def index(self):
        #test()
        body = open("index.html", 'r').read().replace("{GENERATION_COUNT}", str(generation_count)).replace("{CHART1}",chart1).replace("{CHART2}",chart2).replace("{CHART3}", chart3).replace("{CHART4}", chart4).replace("{CHART5}", chart5).replace("{CHART6}", chart6).replace("{CHART7}", chart7)
        return body
    @cherrypy.expose
    def generateChart1(self, **kwargs):
        global chart1
        #randomise_colours()
        #This chart expects the top ten words and their scores. They should be passed in as keyvalue pair
        html = '''
        var ctx = document.getElementById("chart1").getContext("2d");
        var data = {
    labels: {DATALABELS},
    datasets: [
        {
            label: "{LABEL}",
            fillColor: "{COLOUR1}",
            strokeColor: "{COLOUR2}",
            pointColor: "{COLOUR3}",
            pointStrokeColor: "{COLOUR3}",
            pointHighlightFill: "{COLOUR3}",
            pointHighlightStroke: "{COLOUR1}",
            data: {DATA}
        }
    ]
};  
var myBarChart = new Chart(ctx).Bar(data);
myBarChart.update();
'''
        words = []
        scores = []
        for word in kwargs.keys():
            words.append(word)
            scores.append(int(kwargs[word]))
        html=html.replace("{LABEL}", "Top 10 passwords").replace("{DATALABELS}", str(words)).replace("{DATA}", str(scores)).replace("{COLOUR1}", colors[0]).replace("{COLOUR2}", colors[1]).replace("{COLOUR3}", colors[2])
        chart1=html

    @cherrypy.expose
    def generateChart2(self, **kwargs):
        #randomise_colours()
        global chart2
        #This chart expects the top ten words and their scores. They should be passed in as keyvalue pair
        html = '''
        var ctx = document.getElementById("chart2").getContext("2d");
        var data = {
    labels: {DATALABELS},
    datasets: [
        {
            label: "{LABEL}",
            fillColor: "{COLOUR1}",
            strokeColor: "{COLOUR2}",
            pointColor: "{COLOUR3}",
            pointStrokeColor: "{COLOUR3}",
            pointHighlightFill: "{COLOUR2}",
            pointHighlightStroke: "{COLOUR3}",
            data: {DATA}
        }
    ]
};  
var myBarChart = new Chart(ctx).Bar(data);
myBarChart.update();
'''
        words = []
        scores = []
        for word in kwargs.keys():
            words.append(word)
            scores.append(int(kwargs[word]))
        html=html.replace("{LABEL}", "Top 10 passwords").replace("{DATALABELS}", str(words)).replace("{DATA}", str(scores)).replace("{COLOUR1}", colors[0]).replace("{COLOUR2}", colors[1]).replace("{COLOUR3}", colors[2])
        chart2=html


    @cherrypy.expose
    def generateChart3(self, **kwargs):
        global chart3, chart5, generation_count
        #randomise_colours()
        #This chart expects each generation and its how many were cracked
        html = '''
        var ctx = document.getElementById("chart3").getContext("2d");
        var data = {
    labels: {DATALABELS},
    datasets: [
        {
            label: "{LABEL}",
            fillColor: "{COLOUR1}",
            strokeColor: "{COLOUR1}",
            pointColor: "{COLOUR1}",
            pointStrokeColor: "{COLOUR1}",
            pointHighlightFill: "{COLOUR1}",
            pointHighlightStroke: "{COLOUR1}",
            data: {DATA}
        }
    ]
};
options = {datasetFill : false,animation: false,pointDot : false, barStrokeWidth : 5, barStrokeWidth : 2};
var myLineChart = new Chart(ctx).Line(data, options);
myLineChart.update();
document.getElementById('chart3legend').innerHTML = myLineChart.generateLegend();
'''


        values = map(int, kwargs.values())
        score = []
        for i in range(len(values)):
            if i == 0:
                continue
            score.append(values[i])
        html=html.replace("{LABEL}", "Total Cracked Over Time").replace("{DATALABELS}",
         str(range(1,max(map(int, kwargs.keys()))))).replace("{DATA}", str(values)).replace("{COLOUR1}", colors[0]).replace("{COLOUR2}", colors[1]).replace("{COLOUR3}", colors[2])
        chart3=html

        #Now Chart 5
        #This chart is cumulative cracks over all of the generations
        html = '''
        var ctx = document.getElementById("chart5").getContext("2d");
        var data = {
    labels: {DATALABELS},
    datasets: [
        {
            label: "{LABEL}",
            fillColor: "{COLOUR1}",
            strokeColor: "{COLOUR1}",
            pointColor: "{COLOUR1}",
            pointStrokeColor: "{COLOUR1}",
            pointHighlightFill: "{COLOUR1}",
            pointHighlightStroke: "{COLOUR1}",
            data: {DATA}
        }
    ]
};
options = {datasetFill : false,animation: false,pointDot : false, barStrokeWidth : 5, barStrokeWidth : 2 };
var myLineChart = new Chart(ctx).Line(data, options);
myLineChart.update();
document.getElementById('chart3legend').innerHTML = myLineChart.generateLegend();
'''


        values = map(int, kwargs.values())
        score = []
        total = 0
        for i in range(len(values)):
            if i == 0:
                continue
            total+=values[i]
            score.append(total)
        html=html.replace("{LABEL}", "Total Cracked Over Time").replace("{DATALABELS}",
         str(range(1,max(map(int, kwargs.keys()))))).replace("{DATA}", str(score)).replace("{COLOUR1}", colors[0]).replace("{COLOUR2}", colors[1]).replace("{COLOUR3}", colors[2])
        chart5=html

    @cherrypy.expose
    def generateChart4(self, **kwargs):
        #Chart 4 is a chart that 
        global chart4,chart6,chart7, generation_count
        #This chart expects each generation and its score
        #randomise_colours()
        random_colours = []
        #for i in range(len(kwargs.keys())):
            #random_colours.append(colors[i])
        datasettemplate = '''{label: "{LABEL}",
            fillColor: "{COLOUR1}",
            strokeColor: "{COLOUR1}",
            pointColor: "{COLOUR1}",
            pointStrokeColor: "{COLOUR1}",
            pointHighlightFill: "{COLOUR1}",
            pointHighlightStroke: "{COLOUR1}",
            data: {DATA}}'''
        html = '''
        var ctx = document.getElementById("chart4").getContext("2d");
        var data = {
    labels: {DATALABELS},
    datasets: [
        
            {DATASETS}
        
    ]
};

options = {datasetFill : false,animation: false, pointDot : false, barStrokeWidth : 5, barStrokeWidth : 2,showXLabels: 10};
var myLineChart = new Chart(ctx).Line(data, options);

myLineChart.update();
//alert(myLineChart.generateLegend());
document.getElementById('chart4legend').innerHTML = myLineChart.generateLegend();
'''
        results = {}
        max_round = 0
        for i in kwargs.keys():
            value, round = i.split("_")
            if float(round) > max_round:
                max_round = float(round)
            if results.has_key(value):
                results[value].append((float(round), float(kwargs[i])))
            else:
                results[value] = [(float(round), float(kwargs[i]))]

        datasets_html = ""
        count = 0
        for i in results.keys():
            
            tempHtml = datasettemplate[:].replace("{COLOUR1}", colors[count]).replace("{COLOUR2}", colors[count+1]).replace("{COLOUR3}", colors[count+2])
            count +=1
            tempHtml = tempHtml.replace("{LABEL}", i)
            values = []
            for j in sorted(results[i], key=lambda tup: tup[0]):
                values.append(j[1])
            tempHtml = tempHtml.replace("{DATA}", str(values))
            datasets_html += tempHtml +","
        datasets_html = datasets_html[:len(datasets_html)-1]

        generation_count = int(max_round)   
        chart4=html.replace("{DATASETS}",datasets_html).replace("{DATALABELS}", str(map(str, range(0,int(max_round)))))

        #CHART7

        random_colours = []
        #for i in range(len(kwargs)):
            #random_colours.append(colors[i])
        datasettemplate = '''{label: "{LABEL}",
            fillColor: "{COLOUR1}",
            strokeColor: "{COLOUR1}",
            pointColor: "{COLOUR1}",
            pointStrokeColor: "{COLOUR1}",
            pointHighlightFill: "{COLOUR1}",
            pointHighlightStroke: "{COLOUR1}",
            data: {DATA}}'''
        html = '''
        var ctx = document.getElementById("chart7").getContext("2d");
        var data = {
    labels: {DATALABELS},
    datasets: [
        
            {DATASETS}
        
    ]
};

options = {datasetFill : false,animation: false, pointDot : false, barStrokeWidth : 5, barStrokeWidth : 2,showXLabels: 10};
var myLineChart = new Chart(ctx).Line(data, options);

myLineChart.update();
//alert(myLineChart.generateLegend());
document.getElementById('chart4legend').innerHTML = myLineChart.generateLegend();
'''
        results = {}
        max_round = 0
        for i in kwargs.keys():
            value, round = i.split("_")
            if float(round) > max_round:
                max_round = float(round)
            if results.has_key(value):
                results[value].append((float(round), float(kwargs[i])))
            else:
                results[value] = [(float(round), float(kwargs[i]))]

        datasets_html = ""
        count = 0
        number_of_rounds_to_go_back = 25
        colour_count = 0
        for i in results.keys():
            
            tempHtml = datasettemplate[:].replace("{COLOUR1}", colors[colour_count]).replace("{COLOUR2}", colors[colour_count+1]).replace("{COLOUR3}", colors[colour_count+2])
            colour_count +=1
            tempHtml = tempHtml.replace("{LABEL}", i)
            values = []
            count = 0
            for j in sorted(results[i], key=lambda tup: tup[0])[len(results[i])-number_of_rounds_to_go_back:]:
                values.append(j[1])
                count +=1 
                if count > 25:
                    break
            values = values[len(values)-number_of_rounds_to_go_back:]
            tempHtml = tempHtml.replace("{DATA}", str(values))
            datasets_html += tempHtml +","
       

        generation_count = int(max_round)   
        chart7=html.replace("{DATASETS}",datasets_html).replace("{DATALABELS}", str(map(str, range(int(max_round)-number_of_rounds_to_go_back,int(max_round)))))


        ##Chart 6
        datasettemplate = '''{label: "{LABEL}",
            fillColor: "{COLOUR1}",
            strokeColor: "{COLOUR1}",
            pointColor: "{COLOUR1}",
            pointStrokeColor: "{COLOUR1}",
            pointHighlightFill: "{COLOUR1}",
            pointHighlightStroke: "{COLOUR1}",
            data: {DATA}}'''
        html = '''
        var ctx = document.getElementById("chart6").getContext("2d");
        var data = {
    labels: {DATALABELS},
    datasets: [
        
            {DATASETS}
        
    ]
};

options = {datasetFill : false,animation: false, pointDot : false, barStrokeWidth : 5, barStrokeWidth : 2, showXLabels: 10  };
var myLineChart = new Chart(ctx).Line(data, options);

myLineChart.update();

'''
        results = {}
        max_round = 0
        for i in kwargs.keys():
            value, round = i.split("_")
            #if not results_total.has_key(value):
                #results_total[value] = 0
            if float(round) > max_round:    
                max_round = float(round)
            if results.has_key(value):
                #results_total[value]+=float(kwargs[i])
                #results[value].append((round,results_total[value]))
                results[value].append((int(round),float(kwargs[i])))
            else:
                results[value] = [(int(round), float(kwargs[i]))]

        datasets_html = ""
        count = 0
        for i in results.keys():
            tempHtml = datasettemplate[:].replace("{COLOUR1}", colors[count]).replace("{COLOUR2}", colors[count+1]).replace("{COLOUR3}", colors[count+2])
            count +=1
            tempHtml = tempHtml.replace("{LABEL}", i)
            values = []
            total = 0
            print "Should be " + i + " sorted by order : " + str(sorted(results[i], key=lambda tup: tup[0]))
            for j in sorted(results[i], key=lambda tup: tup[0]):
                total+=j[1]
                values.append(total)
            tempHtml = tempHtml.replace("{DATA}", str(values))
            datasets_html += tempHtml +","
        datasets_html = datasets_html[:len(datasets_html)-1]

        generation_count = int(max_round)   
        chart6=html.replace("{DATASETS}",datasets_html).replace("{DATALABELS}", str(map(str, range(0,int(max_round)))))


def randomise_colours():
    global colors
    colors.append("rgba(0,191,255, 0.5)")
    colors.append("rgba(255,0,238, 1)")
    colors.append("rgba(2,250,77, 1)")
    colors.append("rgba(2,35,250, 1)")
    colors.append("rgba(250,2,2, 1)")
    colors.append("rgba(250,233,2, 1)")
    colors.append("rgba(250,2,143, 1)")
    colors.append("rgba(2,250,205, 1)")
    colors.append("rgba(250,126,2, 1)")
    for i in range(100):
        colors.append("rgba({2},{0},{1}, 1)".format(randint(50,153), randint(0,150), randint(5,250)))

randomise_colours()
def redirect():
    raise cherrypy.HTTPRedirect("/index")
#######################################################
#
# Start
#
#######################################################
if __name__ == '__main__':
    #main()'server.socket_host': '0.0.0.0'}
    cherrypy.config.update(
    {'server.socket_host': '0.0.0.0'} ) 
    cherrypy.quickstart(index(), config={

        '/js':
            {'tools.staticdir.on': True,
             'tools.staticdir.dir': os.path.dirname(os.path.realpath(__file__)) + "/js/"
             },

        '/font':
            {'tools.staticdir.on': True,
             'tools.staticdir.dir': os.path.dirname(os.path.realpath(__file__)) + "/font/"
             },

        '/css':
            {'tools.staticdir.on': True,
             'tools.staticdir.dir': os.path.dirname(os.path.realpath(__file__)) + "/css/"
             }
    })

