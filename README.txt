11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111                                                                                            
11              88888888888                            88                                       11
11              88                                     88                                       11
11              88                                     88                                       11
11  8b,dPPYba,  88aaaaa      8b       d8   ,adPPYba,   88  8b       d8   ,adPPYba,  8b,dPPYba,  11
11  88P'   "Y8  88"""""      `8b     d8'  a8"     "8a  88  `8b     d8'  a8P_____88  88P'   "Y8  11
11  88          88            `8b   d8'   8b       d8  88   `8b   d8'   8PP"""""""  88          11
11  88          88             `8b,d8'    "8a,   ,a8"  88    `8b,d8'    "8b,   ,aa  88          11
11  88          88888888888      "8"       `"YbbdP"'   88      "8"       `"Ybbd8"'  88          11
11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111

Revolver is a framework for implementing different algorithms to be used to crack passwords.

A template is provided for the easy creation of new algorithms. All active algorithms are run concurrently allowing them to 'compete' against each other, and the results can be visualised via a web interface where a number of graphs are provided. Rules can be rated according to the number of passwords cracked as well as according to the average complexity of the passwords cracked.

The framework is currently in the alpha stage and we are busy working on ironing out some bugs as well as focussing on efficiency. Our aim is to facilitate the easy generation of useful rules in order to further aid the destruction of passwords.

Flags:
=======
[-P]: name of rule project to create/continue. Projects are stored in the project folder under the name passed in the argument. Example: -P project_1
[-p]: population of initial system, default is 1000. Example: -p 10000
[-H]: path to hash file. Example: -H hashes/hash_list_1
[-W]: path to wordlist file or directory. Example: -W wordlists/wordlist_list_1.file, alternatively: -W wordlists.dir/
[-h]: hashcat type to use for hash list. Example: -h 99999, note the hash file is copied to the project directory and "--remove" is used by default.
[-R]: pass file containing rules to start or update a project. Example: -R T0x1c.rules
[-i]: max number of iterations to cycle. Example: -i 100
[-v]: will start a web server at http://localhost:8080 to visualise the results
[-l]: must point to the hashcat binary to be used. Example: -l "./hashcat/hashcat-cli64.bin"

Usage example:
python main.py -P test1 -p 1500 -i 100 -H hashes/bfield.hash -h 0 -W words.list -v -l './hashcat/hashcat-cli64.bin'