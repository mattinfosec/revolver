# -*- coding: utf-8 -*-

import os
def ReadFromFile(path):
    lines = []
    if os.path.isfile(path):
        read_file = open(path, 'r')
        for line in read_file:
            lines.append(line.rstrip())
        read_file.close()
        return lines
    else:
        return 0

def SaveToFile(path, lines):
    save_file = open(path, 'w')
    for line in lines:
        save_file.write(line + '\n')
    save_file.close()


from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
Base = declarative_base()
import sys
sys.path.insert(0, 'include/')
import db

#project_file = 'db/test_1.db'

def startDB(project_file):
    engine = create_engine('sqlite:///' + project_file, echo=False)
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    global session; session = Session()

#def getAllGeneratedRules():
    #generated_rules_list = session.query(db.GeneratedRule).all()
    #gen_rules = []
    #for rule in generated_rules_list:
        #gen_rules.append(rule.rule)
    #return gen_rules

def getAllSuccessfulRules():
    successful_rules_list = session.query(db.SuccessfulRule).all()
    suc_rules = []
    for rule in successful_rules_list:
        suc_rules.append(rule.rule)
    return suc_rules

import sys
import os
global db_file
global out_name
def GetInput():
    if len(sys.argv) == 1:
        print 'fail'
	quit()
    else:
        for index, argv in enumerate(sys.argv):
	    if argv == '-d':
	      db_file = sys.argv[index + 1]
	    if argv == '-o':
	      out_name = sys.argv[index + 1]
	startDB(db_file)
	SaveToFile(out_name + '.rules', getAllSuccessfulRules())
	#SaveToFile(out_name + '.gen', getAllGeneratedRules())
	
	      
GetInput()