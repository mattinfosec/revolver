import sys
sys.path.insert(0, 'include/')
sys.path.insert(0, 'evolutionary_algorithms/')
import db
import files
from random import randint
import os
from multiprocessing.pool import ThreadPool
import inspect
#from rules import Gene

evolutionary_algorithms = []

#This should get all of the evolutionary algorithms in the evolutionary_algorithms folder
def get_algorithms():
    print "Getting algs"

    global evolutionary_algorithms
    algs=[]
    for subdir, dirs, files in os.walk('evolutionary_algorithms/'):
        for x in files:
            if x.endswith(".py"):
                algs.append(x.split('.')[0])
                print "Found {0}".format(x)
    temp_algs = map(__import__, algs)
    algs=[]
    for i in temp_algs:
        for name, obj in inspect.getmembers(i):
            if inspect.isclass(obj):
                algs.append((name,obj))
    for i in algs:
        try:
            obj = i[1]()
            if obj.active:
                fail = 0
                for alg in evolutionary_algorithms:
                    if obj.name == alg.name:
                        fail = 1
                    else:
                        fail = 0
                if fail == 0:
                    evolutionary_algorithms.append(obj)
                    print "Added algorithm: {0} from evolutionary_algorithms/ ".format(obj.name)
        except:
            pass



def GetRandomRule(length_of_rule):
    simple_rules = ['l', 'c', 'C', 't', 'r', 'd', 'f', '{', '}', '[',']', 'q', 'k', 'K', 'E']
    position_rules = ['TN', 'LN', 'RN', '+N', '-N', '.N', ',N', 'DN', "'N"]
    duplicate_rules = ['yN', 'YN', 'zN', 'ZN', 'pN']
    NM_rules = ['xNM', '*NM']
    X_rules = ['$X', '^X', '@X',')X', '(X']
    XY_rules = ['sXY']#, '*XY']
    NX_rules = ['iNX', 'oNX']
    X_set = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvXxYyZz0123456789!@#$%^&*()_+-=[{}]|\\;"<.>/?~`'
    random_rule = ''
    for j in range(0,length_of_rule):
        random_rules = []
        simple_rule = simple_rules[randint(0,len(simple_rules) - 1)]
        random_rules.append(simple_rule)
        position_rule = position_rules[randint(0,len(position_rules) - 1)]
        position_rule = position_rule[0] + str(randint(0,9))
        random_rules.append(position_rule)
        duplicate_rule = duplicate_rules[randint(0,len(duplicate_rules) - 1)]
        duplicate_rule = duplicate_rule[0] + str(randint(0,9))
        random_rules.append(duplicate_rule)
        NM_rule= NM_rules[0]
        NM_rule = NM_rule[0] + str(randint(0,9)) + str(randint(0,9))
        X_rule = X_rules[randint(0, len(X_rules) - 1)]
        X_rule = X_rule[0] + X_set[randint(0, len(X_set) - 1)]
        random_rules.append(X_rule)
        XY_rule =  XY_rules[randint(0, len(XY_rules) - 1)]
        XY_rule = XY_rule[0] + X_set[randint(0, len(X_set) - 1)] + X_set[randint(0, len(X_set) - 1)]
        random_rules.append(XY_rule)
        NX_rule = NX_rules[randint(0,len(NX_rules) - 1)]
        NX_rule = NX_rule[0] + str(randint(0,9)) + X_set[randint(0, len(X_set) - 1)]
        random_rules.append(NX_rule)
        random_rule += random_rules[randint(0, len(random_rules) - 1)] + " "
    return random_rule.rstrip()

def GenRandomRules(population, lower=1, upper=16):
    random_rules_list = []
    while len(random_rules_list) < population:
        random_rule = GetRandomRule(randint(lower,upper))
        if random_rule not in random_rules_list:
            random_rules_list.append(random_rule)

    return random_rules_list

# def HandleUserRules(config_object): # TODO parse user rules
#     user_rules_list = files.ReadFromFile(config_object.user_rule_file)
#     user_rules_dict = {}
#     for rule in user_rules_list:
#         rule = rule.strip()
#         user_rules_dict[rule] = ['user', config_object.user_rule_file, '']
#     files.SaveToFile(config_object.current_rule_file, user_rules_list)
#     current_rule_list = db.SetCurrentRules(user_rules_dict, config_object)
#
#     return current_rule_list

def HandleUserRules(config_object): # TODO parse user rules
    user_rules_lines = files.ReadFromFile(config_object.user_rule_file)
    user_rules_list = []
    for rule in user_rules_lines:
        rule = rule.strip()
        user_rules_list.append(rule)
    files.SaveToFile(config_object.current_rule_file, user_rules_lines)
    user_rules = {'userrules': user_rules_list}
    current_rule_list = db.SetCurrentRules(user_rules, config_object)

    return current_rule_list



import GeneticAlgorithm

'''TREV:

What this function does, is loop through each of the algorithms in the evolutionary_algorithms folder.
It updates the info on each one, and then asks it to generate rules according to its algorithm.
It first checks that each algorithm gave enough output. If it did, cool, it just adds it to this rounds rules, but if there are too much, it trims the excess off.
If there are too few rules returned, it makes a note of it and will make up the difference by generating random rules.

Finally, all of the rules from all of the algorithms are added to the db for this round.
'''
from random import shuffle

get_algorithms()
class Engine():

    def __init__(self, config_object):
        self.config_object = config_object
        self.successful_rule_objects = db.GetSuccessfulRules()
        

    def getAllSuccessfulRules(self):
        return self.successful_rule_objects[:]

    def getUserRules(self):
        #TODO add this functionality
        return None

    def getAllSuccessfulRules_by_complexity(self):
        x = self.successful_rule_objects[:]
        x.sort(key=lambda x: x.complexity, reverse=True)
        return x

    def Generate(self):
        print 'rules/Generate'
        round_rules = []
        number_of_evolutionary_algorithms = len(evolutionary_algorithms)

        #TODO decide on ratio's of each algorithm to be generated. For now, we just split the total rules wanted by number of algs
        number_for_each_algorithm = self.config_object.population/number_of_evolutionary_algorithms
        random_required = 0
        random_rules = []

        #We're going to save each algorithms rules in this format:
        #   {'algorithm_name' : [rulelist]}
        #In a list. [{'algorithm_name' : [rulelist]}, {'algorithm_name2' : [rulelist]}, {'algorithm_name3' : [rulelist]}]
        # pool = ThreadPool(processes=len(evolutionary_algorithms))
        threads = []
        round_rules = {}
        for i in evolutionary_algorithms:
            #This is a list that contains all of the successful rules that have been seen so far by the main engine
            i.last_round_score = 0.0 #This is a float that represents how well the last set of submitted rules did
            i.user_rules = []
            i.engine = self
            '''t = pool.apply_async(i.generate_rules,(number_for_each_algorithm))
            threads.append((t, i))
        print "Waiting for threads to finish"
        pool.close()
        pool.join()
        print "Threads Finished"
        for i in threads:
            print i
            print i[0]
            print i[0].get
            print i[1]
            quit()
            generated_rules = i[0].get()
            '''
            generated_rules = i.generate_rules(number_for_each_algorithm, self.getAllSuccessfulRules())
            if len(generated_rules) > number_for_each_algorithm:
                generated_rules = generated_rules[:number_for_each_algorithm]
            # generated_rules = db.CheckGenerated(generated_rules)
            round_rules[i.name] = generated_rules

            #If the algorithm does not return enough rules, we simply generate more random ones at the end
            if len(generated_rules) < number_for_each_algorithm:
                random_required += number_for_each_algorithm - len(generated_rules)
            print "Generated {0} rules of type {1}".format(len(generated_rules), i.name)
            #raw_input()
        # round_rules['random'] = GenRandomRules(random_required)
        final_rules = db.SetCurrentRules(round_rules, self.config_object)

        save_rules = []
        for current_rule_obj in final_rules:
            save_rules.append(current_rule_obj.rule)

        shuffle(save_rules)

        files.SaveToFile(self.config_object.current_rule_file, save_rules)

        return final_rules