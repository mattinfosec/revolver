########################################################################################################################
# Handle user input
# ==================
import sys
import os
from multiprocessing import Process
global path_to_hashcat; path_to_hashcat = './hashcat/hashcat-cli64.bin'
def GetInput():
    flags = ['-p', '-h', '-W', '-H', '-P', '-R', '-i', '-v']
    input_dict = {'project_name': '', 'hash_file': '', 'hash_type': '', 'word_list': ['',''], 'user_rule_file': '', 'population': 0, 'max_iteration': 0, 'visualisation':True}
    if len(sys.argv) == 1:
        Help(1)
    else:
        for index, argv in enumerate(sys.argv):
            if argv == '-p':
                try:
                    if sys.argv[index + 1] in flags:
                        Help(1)
                except:
                    Help('-p')
                try:
                    input_dict['population'] = int(sys.argv[index + 1])
                except:
                    Help('-p')
            elif argv == '-l':
	      try:
		if sys.argv[index + 1] in flags:
		  Help(1)
	      except:
		Help('-l')
	      try:
		global path_to_hashcat; path_to_hashcat = sys.argv[index + 1]
	      except:
		Help('-l')
            elif argv == '-v':
                try:
                    p = Process(target=start_web_server, args=())
                    p.start()
                except:
                    Help('-v')

            elif argv == '-h':# TODO check valid hash type is passed
                try:
                    if sys.argv[index + 1] in flags:
                        Help(1)
                    input_dict['hash_type'] = sys.argv[index + 1]
                except:
                    Help('-h')

            elif argv == '-W':# TODO check if word list is a word list
                try:
                    if sys.argv[index + 1] in flags:
                        Help(1)
                    elif os.path.isfile(sys.argv[index + 1]):
                        input_dict['word_list'] = [sys.argv[index + 1], 'file']
                    elif os.path.isdir(sys.argv[index + 1]):
                        # get list of wordlists in directory
                        input_dict['word_list'] = [sys.argv[index + 1], 'dir']
                    else:
                        x = 1 / 0
                except:
                    Help('-W')

            elif argv == '-H':
                try:# TODO check if hash file contains valid hashes
                    if sys.argv[index + 1] in flags:
                        Help(1)
                    else:
                        if os.path.isfile(sys.argv[index + 1]):
                            input_dict['hash_file'] = sys.argv[index + 1]
                        else:
                            x = 1/0
                except:
                    Help('-H')
            elif argv == '-P':
                try:
                    if sys.argv[index + 1] in flags:
                        Help(1)
                    else:
                        input_dict['project_name'] = sys.argv[index + 1]
                except:
                    Help('-P')

            elif argv == '-R':
                try:# TODO parse user passed rule file (individual rules need to be seperated by space)
                    if sys.argv[index + 1] in flags:
                        Help(1)
                    else:
                        if os.path.isfile(sys.argv[index + 1]):
                            input_dict['user_rule_file'] = sys.argv[index + 1]
                        else:
                            x = 1/ 0
                except:
                    Help('-R')
            elif argv == '-i':
                try:
                    if sys.argv[index + 1] in flags:
                        Help(1)
                    else:
                        input_dict['max_iteration'] = int(sys.argv[index + 1])
                except:
                    Help('-i')

    if not input_dict['project_name'] == '':
        return input_dict
    else:
        Help(1)

def start_web_server():
    os.system('cd genetic_algorithm_visualiser/;python main.py >/dev/null 2>&1')
    print "Started webserver on localhost:8080"

def Help(arg):
    if arg == 1:
        print '[-P]: name of rule project to create/continue. Projects are stored in the project folder under the name passed in the argument. Example: -P project_1'
        print '[-p]: population of initial system, default is 1000. Example: -p 10000'
        print '[-H]: path to hash file. Example: -H hashes/hash_list_1'
        print '[-W]: path to wordlist file or directory. Example: -W wordlists/wordlist_list_1.file, alternatively: -W wordlists.dir/'
        print '[-h]: hashcat type to use for hash list. Example: -h 99999, note the hash file is copied to the project directory and "--remove" is used by default.'
        print '[-R]: pass file containing rules to start or update a project. Example: -R T0x1c.rules'
        print '[-i]: max number of iterations to cycle. Example: -i 100'
        print '[-v]: will start a web server at http://localhost:8080 to visualise the results'
        print '[-l]: must point to the hashcat binary to be used. Example: -l "./hashcat/hashcat-cli64.bin"'
        

    if arg == '-p':
        print '[-p]: population of initial system, default is 1000. Example: -p 10000'

    if arg == '-H':
        print '[-H]: path to hash file. Example: -H hashes/hash_list_1'

    if arg == '-W':
        print '[-W]: path to wordlist file or directory. Example: -W wordlists/wordlist_list_1.file, alternatively: -W wordlists.dir/'

    if arg == '-P':
        print '[-P]: name of rule project to create/continue. Projects are stored in the project folder under the name passed in the argument. Example: -P project_1'

    if arg == '-h':
        print '[-h]: hashcat type to use for hash list. Example: -h 99999, note the hash file is copied to the project directory and "--remove" is used by default.'

    if arg == '-R':
        print '[-R]: pass file containing rules to start a project. Example: -R T0x1c.rules'

    if arg == '-i':
        print '[-i]: max number of iterations to cycle. Example: -i 100'
    
    if arg == '-v':
        print '[-v]: Will start a web server at http://localhost:8080 to visualise the results'
    if arg == '-l':
	print '[-l]: must point to the hashcat binary to be used. Example: -l "./hashcat/hashcat-cli64.bin"'

    quit()
