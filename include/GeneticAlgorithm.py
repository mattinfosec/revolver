def Rules(generated_rules_list, successful_rule_objects, population):
    if len(successful_rule_objects) > 0:
        new_rules = {}

        # Algorithm 1: single crossover
        rules_scores = []
        for successful_rule_object in successful_rule_objects:
            rules_scores.append([successful_rule_object.rule, successful_rule_object.score])
        bred_rules_1 = Breed1(rules_scores, generated_rules_list, population)

        # add more algorithms HERE
        # NOTE: append each algorithm's dict to new_rules and return dict containing all algorithms rules with format {rule: [type_name, related_a, related_b]}

        new_rules = bred_rules_1
        return new_rules# return dict: {rule: [type, related_a, related_b]}
    else:
        return 0

# Simple breeding!!!
from random import randint
def Breed1(rules_scores, generated_rules_list, population_max):
    new_rules_parents = []
    new_rules = []
    bred_rules_dict = {}
    if len(rules_scores) < 2:
        return 0
    # Breeding selection process
    couple = []
    couples = []
    while len(rules_scores) > 1:
        chance = randint(0,100)
        lower = 0
        if chance < 50:
            upper = int(len(rules_scores)*0.05)
        else:
            upper = len(rules_scores) - 1
        index = randint(lower, upper)
        parent_X = rules_scores[index]
        rules_scores.remove(parent_X)
        if len(couple) < 2:
            couple.append(parent_X[0])
        elif len(couple) == 2:
            couples.append(couple)
            couple = []; couple.append(parent_X[0])

    # Breeding process
    for couple in couples:
        children = []
        parent_a = couple[0].split(' ')
        parent_b = couple[1].split(' ')
        crossover_a = randint(0, len(parent_a) - 1)
        crossover_b = randint(0, len(parent_b) - 1)
        children.append(parent_a[:crossover_a] + parent_b[crossover_b:])
        children.append(parent_a[crossover_a:] + parent_b[:crossover_b])
        children.append(parent_b[:crossover_b] + parent_a[crossover_a:])
        children.append(parent_b[crossover_b:] + parent_a[:crossover_a])

        for child in children:
            if len(child) < 16 and len(child) > 0 and (' '.join(child)) not in new_rules and (' '.join(child)) not in generated_rules_list:
                new_rules.append(' '.join(child))
                rule_parents = [' '.join(child), ' '.join(parent_a), ' '.join(parent_b)]
                new_rules_parents.append(rule_parents)
                bred_rules_dict[' '.join(child)] = ['breed_1', ' '.join(parent_a), ' '.join(parent_b)]
            if len(new_rules) >= population_max:
                return bred_rules_dict
    return bred_rules_dict
