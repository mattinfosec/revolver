from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func

Base = declarative_base()
#######################################################################################################################
# DB TABLES
class Config(Base):
    __tablename__ = 'config'

    id = Column(Integer, primary_key=True)
    iteration = Column(Integer)
    max_iteration = Column(Integer)
    project_name = Column(String)
    project_pot = Column(String)
    project_file = Column(String)
    project_dir = Column(String)
    hash_file = Column(String)
    hash_type = Column(String)
    wordlist = Column(String)
    wordlist_type = Column(String)
    user_rule_file = Column(String, nullable=True)
    current_rule_file = Column(String)
    debug_file = Column (String)
    population = Column(Integer)
    state = Column(String)

    def __repr__(self):
        return "<Config(id='%s', iteration='%s', max_iteration='%s', project_name='%s', project_pot='%s', project_file ='%s', project_dir='%s', hash_file='%s', hash_type='%s', wordlist='%s', " \
               "wordlist_type='%s', user_rule_file='%s', current_rule_file='%s', debug_file='%s', population='%s', state='%s')>" % \
               (self.id, self.iteration, self.max_iteration, self.project_name, self.project_pot, self.project_file, self.project_dir, self.hash_file, self.hash_type, self.wordlist,
                self.wordlist_type, self.user_rule_file, self.current_rule_file, self.debug_file, self.population, self.state)

class GeneratedRule(Base):
    __tablename__ = 'generatedrule'

    id = Column(Integer, primary_key=True)
    iteration = Column(Integer)
    rule = Column(String, unique=True)
    type = Column(String)
    def __repr__(self):
        return "<GeneratedRule(id='%s', iteration='%s', rule='%s', type='%s')>" % (self.id, self.iteration, self.rule, self.type)


    # related_a = Column(String, nullable=True)
    # related_b = Column(String, nullable=True)

    # def __repr__(self):
    #     return "<GeneratedRule(id='%s', iteration='%s', rule='%s', type='%s', related_a='%s', related_b='%s')>" % (self.id, self.iteration, self.rule, self.type, self.related_a, self.related_b)

def GetGeneratedList():
    generated_rules_object = session.query(GeneratedRule).all()
    generated_rules_list = []
    for generated_rule_object in generated_rules_object:
        generated_rules_list.append(generated_rule_object.rule)

    return generated_rules_list


def UpdateGeneratedRules(current_rules_object):# , generated_rules):
    new_generated = []
    for rule_object in current_rules_object:
        # new_generated.append(GeneratedRule(iteration=rule_object.iteration, rule=rule_object.rule, type=rule_object.type, related_a=rule_object.related_a, related_b=rule_object.related_b))
        new_generated.append(GeneratedRule(iteration=rule_object.iteration, rule=rule_object.rule, type=rule_object.type))
        # generated_rules.append(rule_object.rule)
        # try:
        #     session.delete(rule_object)
        # except:
        #     print 'Cant delete: '
        #     print rule_object

    session.add_all(new_generated)
    session.commit()

class CurrentRule(Base):
    __tablename__ = 'currentrule'

    id = Column(Integer, primary_key=True)
    iteration = Column(Integer)
    rule = Column(String, unique=True)
    type = Column(String)
    # related_a = Column(String, nullable=True)
    # related_b = Column(String, nullable=True)

    def __repr__(self):
        return "<CurrentRule(id='%s', iteration='%s', rule='%s', type='%s')>" % (self.id, self.iteration, self.rule, self.type)

# def SetCurrentRules(rules_dict, config_object):
#     current_rule_list = []
#     for rule in sorted(rules_dict.keys()):
#         current_rule_list.append(CurrentRule(iteration=config_object.iteration, rule=rule, type=rules_dict[rule][0], related_a=rules_dict[rule][1], related_b=rules_dict[rule][2]))
#     session.add_all(current_rule_list)
#     session.commit()
#
#     return current_rule_list
def GetCurrentRules():
    return session.query(CurrentRule).all()

def SetCurrentRules(rules_dict, config_object): #TODO make efficient specifically querying CurrentRule may be redundant
    try:
        current_deleted = session.query(CurrentRule).delete()
        session.commit()
    except:
        session.rollback()
    current_rule_list = []
    unique_rule_list = []
    for key in rules_dict.keys():
        for rule in rules_dict[key]:
            if rule not in unique_rule_list:
                unique_rule_list.append(rule)
    print 'TOTAL GENERATED: ' + str(len(unique_rule_list))
    unique_rule_list = CheckGenerated(unique_rule_list)
    print 'TOTAL UNIQUE: ' + str(len(unique_rule_list))
    for key in rules_dict.keys():
        for rule in rules_dict[key]:
            rule_obj = CurrentRule(iteration=config_object.iteration, rule=rule, type=key)
            if rule in unique_rule_list:
                unique_rule_list.remove(rule)
                try:
                    current_rule_list.append(rule_obj)
                    # session.add(rule_obj)
                    # session.commit()
                except:
                    print '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
                    print rule_obj
                    # session.rollback()
                    # session.commit()
    session.add_all(current_rule_list)
    session.commit()
    return current_rule_list

class SuccessfulRule(Base):
    __tablename__ = 'successfulrule'

    id = Column(Integer, primary_key=True)
    iteration = Column(Integer)
    rule = Column(String, unique=True)
    type = Column(String)
    pruned = Column(String, nullable=True)
    # related_a = Column(String, nullable=True)
    # related_b = Column(String, nullable=True)
    score = Column(Integer)
    complexity = Column(Float)
    def __repr__(self):
        return "<SuccessfulRule(id='%s', iteration='%s', rule='%s', type='%s', score='%s', complexity='%s', pruned='%s')>" % (self.id, self.iteration, self.rule, self.type, self.score, self.complexity, self.pruned)

    # def __repr__(self):
    #     return "<SuccessfulRule(id='%s', iteration='%s', rule='%s', type='%s', related_a='%s', related_b='%s', score='%s')>" % (self.id, self.iteration, self.rule, self.type, self.related_a, self.related_b, self.score)

# UpdateSuccessfulRules(self.config, current_rules_scores_sorted, current_rule_dict, self.successful_rules)
def UpdateSuccessfulRules(config_object, rules_scores_list, rule_dict, rules_complexity_dict, pruned_rules_dict):
    new_successful = []
    type_score_dict = {}
    for rule_score in rules_scores_list:
        current_rule = rule_score[0]
        current_score = rule_score[1]
        current_complexity = rules_complexity_dict[current_rule]/current_score
        if current_rule in rule_dict:
            rule_prop = rule_dict[current_rule] # rules containing ':' will break everything!
            current_type = rule_prop[0]
            if current_type not in type_score_dict:
                type_score_dict[current_type] = current_score
            else:
                type_score_dict[current_type] += current_score
            if current_rule in pruned_rules_dict.keys():
                pruned_rule = pruned_rules_dict[current_rule]
            else:
                pruned_rule = ''
            successful_rule = SuccessfulRule(iteration=config_object.iteration, rule=current_rule, type=current_type, score=current_score, complexity=current_complexity, pruned=pruned_rule)
            # print successful_rule
            new_successful.append(successful_rule)
        # successful_rules_object.append(successful_rule)
    session.add_all(new_successful)
    session.commit()
    return type_score_dict
    # return successful_rules_object

def GetSuccessfulRules():
    successful_rules_list = session.query(SuccessfulRule).order_by(SuccessfulRule.score.desc()).all()
    return successful_rules_list

class Statistic(Base):
    __tablename__ = 'statistic'

    id = Column(Integer, primary_key=True)
    iteration = Column(Integer)
    type = Column(String)
    total_generated = Column(String)
    total_successful = Column(String)
    total_cracked = Column(String)
    round_cracked = Column(String)

    def __repr__(self):
        return "<Statistic(id='%s', iteration='%s', type='%s', total_generated='%s', total_successful='%s', total_cracked='%s', round_cracked='%s')>" % (self.id, self.iteration, self.type, self.total_generated, self.total_successful, self.total_cracked, self.round_cracked)

# GetStatistics(self.config.iteration, generation_types)
# TODO fix accuracy!!!
def UpdateStatistics(iteration, gen_types, round_cracked, type_score_dict):
    current_statistics = []
    test = 0
    for gen_type in gen_types:
        gen_total = len(session.query(CurrentRule).filter_by(iteration=iteration, type=gen_type).all())
        gen_successful = len(session.query(SuccessfulRule).filter_by(iteration=iteration, type=gen_type).all())
        # gen_cracked = session.query(func.sum(SuccessfulRule.score)).filter_by(iteration=iteration, type=gen_type)
        # gen_cracked = gen_cracked.scalar()
        if gen_type in type_score_dict:
            gen_cracked = type_score_dict[gen_type]
            test += gen_cracked
            current_statistics.append(Statistic(iteration=iteration, type=gen_type, total_generated=gen_total, total_successful=gen_successful, total_cracked=gen_cracked, round_cracked=round_cracked))

    for current_statistic in current_statistics:
        current_statistic.round_cracked = test

    session.add_all(current_statistics)
    session.commit()
    return current_statistics

#######################################################################################################################
# DB FUNCTIONS
def startDB(project_file):
    engine = create_engine('sqlite:///' + project_file, echo=False)
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    global session; session = Session()

def HandleConfig(to_do, config_dict):
    startDB(config_dict['project_file'])
    if to_do == 'create':
        print 'db.HandleConfig: creating new project'
        config_obj = Config(iteration=0, max_iteration=config_dict['max_iteration'], project_name=config_dict['project_name'], project_pot=config_dict['project_pot'], project_file=config_dict['project_file'], project_dir=config_dict['project_dir'], hash_file=config_dict['hash_file'], hash_type=config_dict['hash_type'], wordlist=config_dict['word_list'][0], wordlist_type=config_dict['word_list'][1], user_rule_file=config_dict['user_rule_file'], current_rule_file=config_dict['current_rule_file'], debug_file=config_dict['debug_file'], population=config_dict['population'], state='init')
        session.add(config_obj)
        session.commit()
        return config_obj
    elif to_do == 'get':
        print 'db.HandleConfig: continuing existing project'
        config_obj = session.query(Config).order_by(Config.id.desc()).first()
        return config_obj
    elif to_do == 'update_project': # (user_input['word_list'][0] != '' or user_input['user_rule_file'] != '' or user_input['population'] != 0 or user_input['max_iteration'] != 0)
        print 'db.HandleConfig: updating existing project'
        saved_config = session.query(Config).order_by(Config.id.desc()).first()
        change = 0

        if config_dict['user_rule_file'] != '':
            change += 1
        else:
            config_dict['user_rule_file'] = saved_config.user_rule_file
        if config_dict['word_list'][0] == '':# TODO if wordlist changes rerun all generated rules against new wordlist
            config_dict['word_list'][0] = saved_config.wordlist
            config_dict['word_list'][1] = saved_config.wordlist_type
        elif config_dict['word_list'][0] != saved_config.wordlist:
            change += 1
        if config_dict['population'] == 0:
            config_dict['population'] = saved_config.population
        elif config_dict['population'] != 0 and config_dict['population'] != saved_config.population:
            change += 1
        if config_dict['max_iteration'] == 0:
            config_dict['max_iteration'] = saved_config.max_iteration
        elif config_dict['max_iteration'] != 0 and config_dict['max_iteration'] != saved_config.max_iteration:
            change += 1
        if change > 0:
            updated_config = Config(iteration=saved_config.iteration, max_iteration=config_dict['max_iteration'], project_name=config_dict['project_name'], project_pot=config_dict['project_pot'], project_file=config_dict['project_file'], project_dir=['project_dir'], hash_file=saved_config.hash_file, hash_type=saved_config.hash_type, wordlist=config_dict['word_list'][0], wordlist_type=config_dict['word_list'][1], user_rule_file=config_dict['user_rule_file'], current_rule_file=config_dict['current_rule_file'], debug_file=config_dict['debug_file'], population=config_dict['population'], state='init')
            session.add(updated_config)
            session.commit()
            return updated_config

def UpdateIteration(input_config_object):# update iteration after successful iteration, ie after saved all relevant information
    input_config_object.iteration += 1
    session.add(input_config_object)
    session.commit()
    return input_config_object

def UpdateUserRules(input_config_object):# update user rules file if completed iteration and user_rule_file != ''
    new_config = Config(iteration=input_config_object.iteration, max_iteration=input_config_object.max_iteration, project_name=input_config_object.project_name, project_file=input_config_object.project_file, project_dir=input_config_object.project_dir, project_pot=input_config_object.project_pot, hash_file=input_config_object.hash_file, hash_type=input_config_object.hash_type, wordlist=input_config_object.wordlist, wordlist_type=input_config_object.wordlist_type, user_rule_file=input_config_object.user_rule_file, current_rule_file=input_config_object.current_rule_file, debug_file=input_config_object.debug_file, population=input_config_object.population, state=input_config_object.state)
    new_config.user_rule_file = ''
    session.add(new_config)
    session.commit()
    return new_config

def UpdateState(input_config_object, current_state):# use state to resume projects
    input_config_object.state = current_state
    session.add(input_config_object)
    session.commit()
    return input_config_object

# def GET_top_10_root_words():
#     top_words = session.query(SuccessfulWord.root_word, SuccessfulWord.score).order_by(SuccessfulWord.score.desc()).limit(10).all()
#     return top_words
#
def GET_top_10_rules():
    top_rules = session.query(SuccessfulRule.rule, SuccessfulRule.score).order_by(SuccessfulRule.score.desc()).limit(10).all()
    return top_rules
#


def GET_cracked(iteration, pot_file):
    ##total_cracked = len(session.query(Cracked).filter_by(iteration=iteration).all())
    round_cracked = session.query(Statistic).filter_by(iteration=iteration).first()
    if round_cracked == None:
        return 0
    return round_cracked.round_cracked

def GET_statistics(iteration):
    statistics = session.query(Statistic.type, Statistic.total_cracked).filter_by(iteration=iteration).all()
    user_stat = session.query(Statistic.type, Statistic.total_cracked).filter_by(iteration=0, type='userrules').first()
    if user_stat != None and iteration > 0:
        statistics.append(user_stat)
    return statistics

def CheckGenerated(rule_list):
    already_generated = session.query(GeneratedRule).filter(GeneratedRule.rule.in_(rule_list)).all()
    already_generated_list = []
    for al_gen in already_generated:
        already_generated_list.append(al_gen.rule.strip())
    new_unique = []
    not_unique = []
    for rule in rule_list:
        if rule not in already_generated_list and rule not in new_unique:
            new_unique.append(rule)
            # print 'Is unique: ' + rule
        else:
            not_unique.append(rule)
            # print 'Not unique: ' + rule
    return new_unique