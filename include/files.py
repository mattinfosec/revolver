import os
def ReadFromFile(path):
    lines = []
    if os.path.isfile(path):
        read_file = open(path, 'r')
        for line in read_file:
            lines.append(line.rstrip())
        read_file.close()
        return lines
    else:
        return 0

def SaveToFile(path, lines):
    save_file = open(path, 'w')
    for line in lines:
        save_file.write(line + '\n')
    save_file.close()