# Print entire DB
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
Base = declarative_base()
import sys
sys.path.insert(0, 'include/')
import db

project_file = 'projects/test/rules.db'

def startDB(project_file):
    engine = create_engine('sqlite:///' + project_file, echo=False)
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    global session; session = Session()

def printAllConfig():
    print '**************************************************** AllConfig ****************************************************'
    config_list = session.query(db.Config).all()
    for config in config_list:
        print config

def printAllCurrentRules():
    current_rules_list = session.query(db.CurrentRule).all()
    for rule in current_rules_list:
        print rule

def printAllGeneratedRules():
    print '**************************************************** AllGeneratedRules ****************************************************'
    generated_rules_list = session.query(db.GeneratedRule).all()
    for rule in generated_rules_list:
        print rule

def printAllSuccessfulRules():
    print '**************************************************** AllSuccessfulRules ****************************************************'
    successful_rules_list = session.query(db.SuccessfulRule).all()
    for rule in successful_rules_list:
        print rule

def printAllSuccessfulWords():
    print '**************************************************** AllSuccessfulWords ****************************************************'
    successful_words_list = session.query(db.SuccessfulWord).all()
    for word in successful_words_list:
        print word

def printAllCracked():
    print '**************************************************** AllCrackedPasswords ****************************************************'
    cracked_list = session.query(db.Cracked).all()
    for password in cracked_list:
        print password

def printAllStatistics():
    print '**************************************************** AllStatistics ****************************************************'
    stats = session.query(db.Statistic).all()
    for stat in stats:
        print stat

startDB(project_file)
# printAllCurrentRules()
# printAllGeneratedRules()
printAllSuccessfulRules()
printAllStatistics()
printAllConfig()